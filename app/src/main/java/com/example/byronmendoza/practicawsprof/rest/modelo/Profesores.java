package com.example.byronmendoza.practicawsprof.rest.modelo;

import com.google.gson.annotations.SerializedName;

public class Profesores {

    @SerializedName("id")
    private String id;

    @SerializedName("cedula")
    private String cedula;

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("apellido")
    private String apeliido;

    @SerializedName("titulo")
    private String titulo;

    @SerializedName("materia")
    private String materia;

    @SerializedName("imagen")
    private String imagen;



    public String getId() {
        return id;
    }

    public String getCedula() {
        return cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApeliido() {
        return apeliido;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getMateria() {
        return materia;
    }

    public String getImagen() {
        return imagen;
    }
}

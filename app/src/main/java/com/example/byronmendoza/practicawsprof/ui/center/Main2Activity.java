package com.example.byronmendoza.practicawsprof.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.byronmendoza.practicawsprof.R;
import com.example.byronmendoza.practicawsprof.rest.adapter.ProfesoresAdapter;
import com.example.byronmendoza.practicawsprof.rest.modelo.Profesores;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    TextView ide, nombres2, apellidos2, cedula2, materia2, titulo2;

    ImageView img2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String id = getIntent().getStringExtra("id");

        ide = (TextView)findViewById(R.id.IdProf2);
        nombres2 = (TextView)findViewById(R.id.TxtNombre2);
        apellidos2 = (TextView)findViewById(R.id.TxtApellido2);
        cedula2 = (TextView)findViewById(R.id.TxtCedula2);
        materia2 = (TextView)findViewById(R.id.TxtMateria2);
        titulo2 = (TextView)findViewById(R.id.TxtTitulo2);
        img2 = (ImageView)findViewById(R.id.ImgProf2);

        Estmostrar2(id);


    }

    private void Estmostrar2(String id){
        ProfesoresAdapter profesoresAdapter = new ProfesoresAdapter();
        Call<Profesores> profesoresCall = profesoresAdapter.getProfesor(id);
        profesoresCall.enqueue(new Callback<Profesores>() {
            @Override
            public void onResponse(Call<Profesores> call, Response<Profesores> response) {

                Profesores profesores = response.body();
                Log.e("PROFESORES", profesores.getNombre());

                nombres2.setText("NOMBRES: " + profesores.getNombre().toString());
                apellidos2.setText("APELLIDOS: " + profesores.getApeliido().toString());
                cedula2.setText("PARCIAL 1: " + profesores.getCedula().toString());
                materia2.setText("PARCIAL 2: " + profesores.getMateria().toString());
                titulo2.setText("APRUEBA: " + profesores.getTitulo().toString());
                Picasso.get().load(profesores.getImagen()).into(img2);

            }

            @Override
            public void onFailure(Call<Profesores> call, Throwable t) {

            }
        });
    }
}

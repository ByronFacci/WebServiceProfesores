package com.example.byronmendoza.practicawsprof.rest.service;


import com.example.byronmendoza.practicawsprof.rest.constants.ApiConstanst;
import com.example.byronmendoza.practicawsprof.rest.modelo.Profesores;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TeachersServices {

    @GET(ApiConstanst.PROFESORES)
    Call<List<Profesores>> getProfesores();

    @GET(ApiConstanst.PROFESOR)
    Call<Profesores> getProfesor(@Path("id") String id);
}

package com.example.byronmendoza.practicawsprof.rest.constants;

public class ApiConstanst {

    public static final String BASE_PROFESORES_URL="http://159.65.168.44:3002/";

    public static final String PROFESORES= "profesores/";

    public static final String PROFESOR = "profesor/{id}";
}

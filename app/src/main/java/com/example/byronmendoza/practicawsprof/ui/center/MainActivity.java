package com.example.byronmendoza.practicawsprof.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.byronmendoza.practicawsprof.R;
import com.example.byronmendoza.practicawsprof.rest.adapter.ProfesoresAdapter;
import com.example.byronmendoza.practicawsprof.rest.modelo.Profesores;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView ide, nombres2, apellidos2, parcial12, parcial22, aprueba2;

    ImageView img2;

    ArrayList<Profesores> ListaProfesor;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListaProfesor=new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.Rprofesores);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        EstMostrarprf();

    }

    private void EstMostrarprf() {

        ProfesoresAdapter estudiantesAdapter = new ProfesoresAdapter();
        Call<List<Profesores>> call = estudiantesAdapter.getProfesores();
        call.enqueue(new Callback<List<Profesores>>() {
            @Override
            public void onResponse(Call<List<Profesores>> call, Response<List<Profesores>> response) {

                List<Profesores> list = response.body();

                for (Profesores profesores: list){
                    ListaProfesor.add(profesores);
                }
                AdaptadorProfesores adaptadorProfesores = new AdaptadorProfesores(ListaProfesor);
                recyclerView.setAdapter(adaptadorProfesores);
            }

            @Override
            public void onFailure(Call<List<Profesores>> call, Throwable t) {

            }
        });

    }
}
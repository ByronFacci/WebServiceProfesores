package com.example.byronmendoza.practicawsprof.rest.adapter;


import com.example.byronmendoza.practicawsprof.rest.constants.ApiConstanst;
import com.example.byronmendoza.practicawsprof.rest.modelo.Profesores;
import com.example.byronmendoza.practicawsprof.rest.service.TeachersServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public class ProfesoresAdapter extends BaseAdapter implements TeachersServices{

    private TeachersServices teachersServices;

    public ProfesoresAdapter(){
        super(ApiConstanst.BASE_PROFESORES_URL);
        teachersServices=createService(TeachersServices.class);
    }

    @Override
    public Call<List<Profesores>> getProfesores() {
        return teachersServices.getProfesores();
    }
    @Override
    public Call<Profesores> getProfesor(String id) {
        return teachersServices.getProfesor(id);
    


}




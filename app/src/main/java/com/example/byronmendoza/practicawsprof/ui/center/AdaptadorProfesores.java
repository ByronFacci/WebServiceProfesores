package com.example.byronmendoza.practicawsprof.ui.center;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.byronmendoza.practicawsprof.R;
import com.example.byronmendoza.practicawsprof.rest.modelo.Profesores;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorProfesores  extends RecyclerView.Adapter<AdaptadorProfesores.ViewProfesores>{

    ArrayList<Profesores> Prf;

    public AdaptadorProfesores(ArrayList<Profesores> Prf){
        this.Prf = Prf;
    }

    @NonNull
    @Override
    public ViewProfesores onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);

        return new ViewProfesores(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewProfesores holder, final int position) {

        holder.id.setText("ID: " + Prf.get(position).getId());
        holder.Nombres.setText("NOMBRES: "+ Prf.get(position).getNombre());
        holder.Apellidos.setText("APELLIDOS " + Prf.get(position).getApeliido());
        holder.Cedula2.setText("CEDULA: " + Prf.get(position).getCedula());
        holder.Materia2.setText("MATERIA: " + Prf.get(position).getMateria());
        holder.Titulo2.setText("TITULO: " + Prf.get(position).getTitulo());

        Picasso.get().load(Prf.get(position).getImagen()).into(holder.imagen);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, Main2Activity.class);
                intent.putExtra("id", Prf.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Prf.size();

    }

    public class ViewProfesores extends RecyclerView.ViewHolder{

        TextView id, Nombres, Apellidos, Cedula2, Materia2, Titulo2;
        ImageView imagen;
        LinearLayout linearLayout;

        public ViewProfesores(View itemView) {
            super(itemView);

            id = (TextView)itemView.findViewById(R.id.IdEProf);
            Nombres = (TextView)itemView.findViewById(R.id.TxtNombre);
            Apellidos = (TextView)itemView.findViewById(R.id.TxtApellido);
            Cedula2 = (TextView)itemView.findViewById(R.id.TxtCedula);
            Materia2 = (TextView)itemView.findViewById(R.id.TxtMateria);
            Titulo2 = (TextView)itemView.findViewById(R.id.TxtTitulo);
            imagen = (ImageView)itemView.findViewById(R.id.ImgEst);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.Linear);
        }
    }
}
